//Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

//This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Database connection
//Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://vrain4:NGZYfea1qlVfFGwd@cluster0.tosyxsr.mongodb.net/b281_to-do?retryWrites=true&w=majority",

		{

			useNewUrlParser:true,
			useUnifiedTopology: true
		}

);

//Add the task route
// Allows all the task routes created in the "taskRoute.js" file use "/tasks" route

app.use("/tasks", taskRoute);




//Server Listening

if(require.main === module){

	app.listen(port, () => console.log(`Server running at port ${port}`));

}

module.exports = app;