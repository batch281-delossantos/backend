//JavaScript Operators

// Arithmetic Operators


let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let prodduct = x * y;
console.log("Result of multiplication operator: " + prodduct);

let qoutient = x / y;
console.log("Result of division operator: " + qoutient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);


//Assignment Operator (=) - assigns the value of the right operand to a variable

let assignmentNumber = 8;

//Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;

console.log("Result of addition assignment Operator: " + assignmentNumber);
assignmentNumber+=2;

console.log("Result of addition assignment Operator: " + assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator(-=,*=,/=)

assignmentNumber -= 2;

console.log("Result of subtraction assignment Operator: " + assignmentNumber);

assignmentNumber *= 2;

console.log("Result of multiplication assignment Operator: " + assignmentNumber);


assignmentNumber /= 2;

console.log("Result of division assignment Operator: " + assignmentNumber);

//Multiple Operators and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

// Using Parentheses

let pemdas = 1 + (2-3)*(4/5);
console.log("Result of pemdas operation: " + pemdas);

pemdas = (1+(2-3))*(4/5);

console.log("Result of pemdas operation: " + pemdas);

//Increment and Decrememt

let z = 1;

//pre-increment

let increment = ++z;
console.log("Result of pre-increment: "+ z);
console.log("Result of pre-increment: "+ increment);

// post increment

increment = z++;
console.log("Result of post-increment: "+ increment);
console.log("Result of post-increment: "+ z);




// Type Coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);	

let numC = false + 1;
console.log(numC);


// Comparison Operators

let juan = 'juan';

// Equality Operator (==)

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' ==  juan);

//Inequality Operator (!=)

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' !=  juan);

// Strict Equality Operator ( === )

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' ===  juan);


// Strict Inequality Operator (!==)

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !==  juan);


// Relational Operators

let a = 50;
let b = 65;

// Greater than(>)

let isGreaterThan = a > b;
// less than (<)
let isLessThan = a < b;
// Greater than or Equal (>=)
let isGTorEqual = a >= b;
// less than or equal
let isLTorEqual = a <=b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);


//Logical Operators

let isLegalAge = true;
let isRegistered = false;

// Logical adn Operator (&&)
// Returns true if all operands are true

let allRequirementsMet = isLegalAge && isRegistered;

console.log("Result of logical AND operator: " + allRequirementsMet);

// Logical Or Operator (||)
// Returns true if one of the operands are true

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);


// Logical Not Operator (!)
// Returns the opposite value

let someRequirementsNotMet = !isRegistered
console.log("Result of logical NOT operator: " + someRequirementsNotMet);

