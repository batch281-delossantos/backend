// Function with parameters

function printName(name){

	console.log("My name is "+ name);
}

printName("Arvin");

let sampleName ="Vrain";

printName(sampleName);

function checkDivisibilityBy8(num){

	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is:"+ remainder);

	let isDivisibleBy8 = remainder == 0;
	console.log("Is "+ num + "disivible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(8);

// Functions as Arguments

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);

// Using multiple parameters
function createFullname(firstName,middleName,lastName){

	console.log(firstName+' '+middleName+' '+lastName);
}

createFullname("Arvin","Figuerra","de los Santos");

// Using variables as arguments

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";


createFullname(firstName,middleName,lastName);


// Return statement - allows us to output a value from a function to be passed to the line/block of code that invoked the function

function returnFullName(firstName, middleName , lastName){

		return firstName + ' ' + middleName + ' ' + lastName;
		console.log("This message will not be printed");
}

let completeName = returnFullName("Jeffry","Smith","Bezos");
console.log(completeName);
console.log(returnFullName(lastName,middleName,lastName));

function returnAddress(city, country){
	let fullAddress = city + ', '+country;
	return fullAddress;
}

let myAdress = returnAddress("Dasma","Philippines");
console.log(myAdress)