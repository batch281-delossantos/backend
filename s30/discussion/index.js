db.fruits.insertMany([
	{
		"name": "Apple",
		"color": "Red",
		"stock": 20,
		"price": 40,
		"supplier_id" : 1,
		"onSale": true,
		"origin": ["Philippines", "US"]
	},
	{
		"name": "Banana",
		"color": "Yellow",
		"stock": 15,
		"price": 20,
		"supplier_id" : 2,
		"onSale": true,
		"origin": ["Philippines", "Ecuador"]
	},
	{
		"name": "Kiwi",
		"color": "Green",
		"stock": 25,
		"price": 50,
		"supplier_id" : 1,
		"onSale": true,
		"origin": ["US", "China"]
	},
{
		"name": "Mango",
		"color": "Yellow",
		"stock": 10,
		"price": 120,
		"supplier_id": 2,
		"onSale": false,
		"origin": ["Philippines", "India"]
	}
]);

// MongoDB Aggregation

/*
	-Used to generate manipulate data and perfom operations to create filtered results

	- "$match" - used to pass documents that meet specified conditions

	- Syntax 
		- {$match: {field: value}}
	

	- $group - used to group elements together and field-value pairs using the data from the grouped elements

	- Syntax
		db.collectionName:aggregate([

			{$match:{field:value}},
			{$group: {_id: "fielddB"}, {result:{operation}
			}}

		])
*/

db.fruits.aggregate([
 	{ $match: { "onSale": true }},
 	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
]);


// Field Projection with aggregation

/*
	$project - used when aggregating data to include/exclude field from the returned results
	Syntax:

		- {$project: {field:1/0}}	

*/

db.fruits.aggregate([
 	{ $match: { "onSale": true }},
 	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
 	{ $project: {_id:0}}
]);

// Sorting aggregated results

/*
	
	$sort - used to change the order of aggregated result
	Syntax:

		{$sort:{field: 1/-1}}

*/

db.fruits.aggregate([
 	{ $match: { "onSale": true }},
 	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
 	{ $sort: {total:-1}}

]);

db.fruits.aggregate([
 	{$unwind:"$origin"},
        {$group:{_id:"$origin",kinds:{$sum:1}}}
]);


//One-to-one Relationship
//Create an id and stores it in the variable owner

var owner = ObjectId();

//Creates am "owner" document that uses the generate id

db.owners.insertOne({

	_id: owner,
	name: "John Smith",
	contact: "09951019781"
})

// Change the "owner_id" using the actual id in the previously created document

db.suppliers.insert({

	name: "ABC Fruits",
	contact: "09951019781",
	owner_id: ObjectId("646b5d2309c83584c887ac82")

})

// One-to-few relationship

db.suppliers.insertOne({
	name: "DEF Fruits",
	contact: "09878787878",
	addresses:[
		{street: "123 San Jose St", city:"Manila"},
		{street: "367 Gil Puyat", city:"Makati"}
	]


})

// One-to Many Relationship

var supplier = ObjectId();
var branch1 = ObjectId();
var branch2 = ObjectId();

db.suppliers.insertOne({
	_id: supplier,
	name: "GHI Fruits",
	contact: "0239392233",
	branches:[
		branch1
	]


})

db.branches.insertOne({
	_id: branch1,
	name: "BF Homes",
	address: "123 Arcardio Santos St.",
	city: "Paranaque",
	supplier_id: ObjectId("646b61f109c83584c887ac87")

})

db.branches.insertOne({
	_id: branch2,
	name: "Rizal",
	address: "123 San Jose St.",
	city: "Manila",
	supplier_id: ObjectId("646b61f109c83584c887ac87")

})
