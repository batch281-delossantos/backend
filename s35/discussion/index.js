const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;



//MongoDB Connection

mongoose.connect("mongodb+srv://vrain4:NGZYfea1qlVfFGwd@cluster0.tosyxsr.mongodb.net/b281_to-do?retryWrites=true&w=majority",

		{

			useNewUrlParser:true,
			useUnifiedTopology: true
		}


);

// Set notification for connection success or failure

let db = mongoose.connection;

//If a connection error occurred, output in the console
db.on("error",console.log.bind(console,"connection error"));

//If the connection is successful, output in the console

db.once("open", () => console.log("We're connected to the cloud database"));


//Mongoose Schemas
const taskSchema = new mongoose.Schema({

	name: String,
	status: {

		type: String,
		default: "pending"
	}

})

// Models [Task models]
const Task = mongoose.model("Task",taskSchema);

// Allows the app to read json data
app.use(express.json());
// Allows the app to read data forms
app.use(express.urlencoded({extended:true}));

// Creation of todo list routes

// Creating a new task

app.post("/tasks", (req,res) =>{

	//IF a document was found and document's name matches the information from the client

	Task.findOne({name: req.body.name}).then((result, err) =>{

		if(result !==null && result.name === req.body.name){

			//Return a message to the client/Postman
			return res.send("Duplicate task found");
		}
		//IF no document was found
		else{
			// Create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name

			})

			newTask.save().then((saveTask, saveErr) =>{

				//If there are errors in saving
				if(saveErr){

					return console.error(saveErr);

				}
				else{
					return res.status(201).send("New Task created")
				}

			})

		}

	})
});

//Get all the tasks

app.get("/tasks", (req,res) =>{

	Task.find({}).then((result,err)=>{
		//If an error occurred

		if(err){

			//will print any errors found in the console
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}

	})

})



// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));