// CRUD Operations

// Create 


db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"contact":{
		"phone":"888888888",
		"email":"janedoe@mail.com"
	},
	"course":["CSS","Javascript","Python"],
	"department":"none"

});

// Insert many collections

db.users.insertMany([

	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76,
		"contact":{
			"phone":"555555",
			"email":"stephen@mail.com"
		},
		"course":["React","Javascript","Python"],
		"department":"none"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"contact":{
			"phone":"12412141",
			"email":"neil@mail.com"
		},
		"course":["React","Laravel","Python"],
		"department":"none"
	}
]);

//Find documents (Read)
//Find

// Retrieving all documents

db.users.find();




// Retrieving single documents
db.users.find({"firstName":"Stephen"});


//Retrieving documents with multiple parameters
db.users.find({"lastName": "Armstrong", "age":82});

//Updating documents(Update)

//Update a single document to update

//Creating a document to update

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 21,
	"contact":{
		"phone":"0000000",
		"email":"janedoe@mail.com"
	},
	"course":[""],
	"department":"none"

});

db.users.updateOne(	
	{"firstName": "Test"},
	{

		$set : {

		"firstName": "Bill",
		"lastName": "Gates",
		"age": 65,
		"contact":{
			"phone":"42535252",
			"email":"bill@mail.com"
		},
		"course":["PHP","Laravel","HTML"],
		"department":"Operations",
		"status": "active"

		}
	}


)

//Updating multiple documents
db.users.updateMany(
	{"department": "none"},
	{
		$set : {"department":"HR"}
	}

)

// Replace One

db.users.replaceOne(
	{"firstName": "Bill"},

	{

		"firstName": "Bill",
		"lastName": "Gates",
		"age": 65,
		"contact":{
			"phone":"42535252",
			"email":"bill@mail.com"
		},
		"course":["PHP","Laravel","HTML"],
		"department":"Operations"
		
	}

)

// Deleting documents(Delete)

//Creating a document to delete

db.users.insertOne({
	"firstName": "Arvin"

}

)

db.users.deleteOne({
	"firstName":"Arvin"
});