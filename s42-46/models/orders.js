const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	
	userID:{
		type : String,
		required : [true, "Product ID is required"]
	},
	orderID:{
		type : String,
		required : [true, "Product ID is required"]
	},
	orderDetails : [
		{
			productId : {
				type : String,
				required : [true, "Product ID is required"]
			},
			productName:{
				type : String,
				required : [true, "Product Name is required"]

			},
			purchasedOn : {
				type : Date,
				default : new Date()
			},
			quantity : {
				type : Number,
				default : new Number()
			},
			
		}
	],
	totalAmount:{
				type : Number,
				default : new Number()
	}
})

module.exports = mongoose.model("User", userSchema);
