const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	orders : [
		{
			productId : {
				type : String,
				required : [true, "Product ID is required"]
			},
			productName:{
				type : String,
				required : [true, "Product Name is required"]

			},
			purchasedOn : {
				type : Date,
				default : new Date()
			},
			quantity : {
				type : Number,
				default : new Number()
			},
			totalAmount:{
				type : Number,
				default : new Number()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);
