const Product = require("../models/products");

module.exports.addProduct = (data) =>{


	let newProduct = new Product({
		productName : data.product.productName,
		description : data.product.description,
		price : data.product.price,
		quantity: data.product.quantity
	});

	return newProduct.save().then((product, error) => {
		if(error){
			return false;

		}else {
			return true;
		};
	})
};

// Retrieve all products
module.exports.getAllProduct = () =>{

		return Product.find({}).then(result =>{

			return result;
		})

}

module.exports.getAllActive = () =>{

		return Product.find({isActive:true}).then(result =>{

			return result;
		})

}

module.exports.getProduct = (reqParams) =>{

		return Product.findById(reqParams.productId).then(result =>{

			return result;
		})

}

// Update a product
module.exports.updateProduct = (reqParams,reqBody) =>{

	//Specify the fields/properties of the document to be updated
	let updatedProduct = {

		productName: reqBody.productName,
		description : reqBody.description,
		price : reqBody.price,
		quantity: reqBody.quantity,
		
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error)=>{

		//Product not updated

		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Archive a product
module.exports.archiveProduct = (reqParams,reqBody) =>{

	//Specify the fields/properties of the document to be updated
	let archivedProduct = {

		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product,error)=>{

		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// unarchive a product
module.exports.archiveProduct = (reqParams,reqBody) =>{

	//Specify the fields/properties of the document to be updated
	let archivedProduct = {

		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product,error)=>{

		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}