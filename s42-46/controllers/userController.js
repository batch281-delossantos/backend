const User = require("../models/users");
const Product = require("../models/products");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User Registration
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The find method returns a record if a match is found
		if(result.length > 0){
			return true;

		// No duplicate emails found
		// The user is not registered in the database
		} else {
			return false;
		};
	});
};

// User registration
module.exports.registerUser = (reqBody) => {

	// Creates a variable named "newUser" and insantiates a new "User" object using the Mondoose model
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		// User registration failed
		if(error){
			return false;

		// User registration successful
		} else {
			return true;
		}
	})

};


// User authentication
module.exports.loginUser = (reqBody) => {
	
	return User.findOne({email : reqBody.email}).then(result => {

		
		if(result == null){
			return false

		
		} else {
		
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			

		
			if(isPasswordCorrect){
				// Generate an access token
				// Uses the createAccessToken method defined in the auth.js file
				return { access: auth.createAccessToken(result)}

			// Passwords do not match
			} else {
				return false;
			}
		}
	})
};

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};




module.exports.createOrder = (data) =>{

	return User.findById(data.userId).then(user =>{

		user.orders.push({

			productId:data.productId,
			productName: data.productName,
			quantity:data.quantity,
			totalAmount:data.totalAmount


		});
		return user.save().then((user,error) =>{

			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})



}



