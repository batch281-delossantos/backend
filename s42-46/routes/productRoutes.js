const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product
router.post("/", auth.verify, (req, res) => {
	
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));

	}
	else{
		res.send(false);
	}

});

// Retrieving all products

router.get("/all", (req,res) =>{


productController.getAllProduct()
.then(resultFromController => res.send(resultFromController))

})

router.get("/activeProducts", (req,res) =>{


productController.getAllActive()
.then(resultFromController => res.send(resultFromController))

})

//retrieve a product

router.get("/:productId", (req, res) =>{

	console.log(req.params.productId);
	productController.getProduct(req.params)
	.then(resultFromController => res.send(resultFromController))

})

// Route for updating a product (Admin)

router.put("/:productId", auth.verify, (req,res)=>{



	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){

	productController.updateProduct(req.params, req.body)
	.then(resultFromController => res.send(resultFromController))

	}
	else{
		res.send(false);
	}

})


// Route for archiving a product

router.patch("/:productId/archive", auth.verify, (req,res)=>{



	const data = {
		products: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){

	productController.archiveProduct(req.params, req.body)
	.then(resultFromController => res.send(resultFromController))

	}
	else{
		res.send(false);
	}

})

router.patch("/:productId/unarchive", auth.verify, (req,res)=>{



	const data = {
		products: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){

	productController.archiveProduct(req.params, req.body)
	.then(resultFromController => res.send(resultFromController))

	}
	else{
		res.send(false);
	}

})

// Allows us to export the "router" object that will be accessed in our index.js file
module.exports = router;