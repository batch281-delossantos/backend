const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// Route for user registration
router.post("/register", (req, res) => {
	
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


// Route for get user details

router.get("/details", auth.verify, (req, res) => {

	// Uses decode method defined in the auth.js file to retrieve user information from the token passing the "token" from the request header
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});




// Route for checkout
router.post("/checkout", auth.verify,(req,res) =>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity,
		totalAmount: req.body.totalAmount

	}

	if(data.isAdmin == false){
	userController.createOrder(data)
	.then(resultFromController =>res.send(resultFromController))

	}
	else{
		res.send(false);
	}

})




module.exports = router;